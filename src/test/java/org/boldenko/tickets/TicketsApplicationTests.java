package org.boldenko.tickets;

import org.boldenko.tickets.web.TicketController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebMvcTest(TicketController.class)
@RunWith(SpringRunner.class)
class TicketsApplicationTests {

    /*
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TicketController ticketController;


    @Test
    public void contextLoads() {
        assertNotNull(restTemplate);
        assertNotNull(ticketController);

        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:"+port+"/api/ticket", String.class);

        assertEquals("Hello?", response.getBody());
    }

    @Test
    public void simpleTest(){
    }

     */


    @Autowired
    private MockMvc mockMvc;


    @Test
    @WithMockUser(username = "admin", roles = {"admin"})
    public void test() throws Exception {

        mockMvc.perform(get("/api/ticket")).
                andExpect(status().isOk()).
                andExpect(content().string("Hello?"));
    }

    @Test
    @WithMockUser(username = "user", roles = {"user"})
    public void test1() throws Exception {
        mockMvc.perform(get("/api/ticket")).
                andExpect(status().isOk()).
                andExpect(content().string("Goodbye!"));

    }


    @Test
    @WithMockUser(username = "dmitry", roles = {"user"})
    public void createTicketTest() {

        //mockMvc.perform(post("api/ticket").content("[]"))
    }
}
