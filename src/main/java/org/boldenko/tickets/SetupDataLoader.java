package org.boldenko.tickets;

import lombok.RequiredArgsConstructor;
import org.boldenko.tickets.access.Role;
import org.boldenko.tickets.access.RoleRepository;
import org.boldenko.tickets.access.User;
import org.boldenko.tickets.access.UserRepository;
import org.boldenko.tickets.data.Ticket;
import org.boldenko.tickets.data.TicketRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final TicketRepository ticketRepository;

    private final PasswordEncoder passwordEncoder;

    private boolean setUp = false;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (setUp)
            return;

        populateWithSomeData();

        setUp = true;
    }

    public void populateWithSomeData() {
        Role adminRole = createRoleIfNotExist(Role.ADMIN);
        Role operatorRole = createRoleIfNotExist(Role.OPERATOR);
        Role userRole = createRoleIfNotExist(Role.USER);
        // у role есть коллекция юзеров, как динамически обновлять таблицу join'ов ? как извлекать юзеров из роли по требованию?

        User admin = new User();
        admin.setRoles(Arrays.asList(adminRole));
        admin.setUsername("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        userRepository.save(admin);

        User dmitry = new User();
        dmitry.setRoles(Arrays.asList(userRole));
        dmitry.setUsername("dmitry");
        dmitry.setPassword(passwordEncoder.encode("pass"));
        userRepository.save(dmitry);

        User oper = new User();
        oper.setUsername("oper");
        oper.setPassword(passwordEncoder.encode("oper"));
        oper.setRoles(Arrays.asList(operatorRole));
        userRepository.save(oper);

        Ticket ticket1 = new Ticket();
        ticket1.setArticle("У меня вышла черепаха");
        ticket1.setOwner(dmitry);

        ticketRepository.save(ticket1);

        Ticket ticket2 = new Ticket();
        ticket2.setArticle("Течет кран и не перестает!");
        ticket2.setOwner(dmitry);
        ticketRepository.save(ticket2);

        Ticket ticket3 = new Ticket();
        ticket3.setOwner(dmitry);
        ticket3.setStatus(Ticket.Status.SENT);
        ticket3.setArticle("Do you love me again?");
        ticketRepository.save(ticket3);
    }

    @Transactional
    Role createRoleIfNotExist(String name) {
        Optional<Role> found = roleRepository.findByName(name);
        if (found.isEmpty()) {
            Role newRole = new Role();
            newRole.setName(name);
            roleRepository.save(newRole);
            return roleRepository.save(newRole);
        } else
            return found.get();
    }
}


