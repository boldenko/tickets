package org.boldenko.tickets.web;

import lombok.RequiredArgsConstructor;
import org.boldenko.tickets.access.Role;
import org.boldenko.tickets.access.User;
import org.boldenko.tickets.access.UserRepository;
import org.boldenko.tickets.data.Ticket;
import org.boldenko.tickets.data.TicketRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/ticket")
@RequiredArgsConstructor
public class TicketController {

    private final TicketRepository ticketRepository;

    private final UserRepository userRepository;

    /*
        Для пользователя возвращает созданные им заявки
        Для оператора возвращает заявки, переданные на рассмотрение
        Для админа не возвращает ничего
     */
    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Ticket>> get(HttpServletRequest request) {
        if (request.isUserInRole(Role.ADMIN))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        else {
            request.getRemoteUser();

            Optional<User> found = userRepository.findByUsername(request.getRemoteUser());
            // Возможно ли не найти?
            if (found.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            else {
                User user = found.get();
                if (request.isUserInRole(Role.USER))
                    return new ResponseEntity<>(getUserTickets(user), HttpStatus.OK);
                else
                    return new ResponseEntity<>(getActiveTickets(), HttpStatus.OK);
                    //return new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
            }
//            if (request.isUserInRole(Role.USER)) {
//                return new ResponseEntity<>(HttpStatus.OK);
//            }
//            return new ResponseEntity<>(HttpStatus.OK);
            //return new ResponseEntity<>(ticketRepository.findAllByOwner(userRepository.findByUsername(principal.getName())));
        }

//        return new ResponseEntity<>(HttpStatus.OK);
        //request.





//        Ticket ticket = new Ticket();
//        ticket.setArticle("У меня вышла черепаха");
//        return List.of(ticket);

        //  System.out.println(request.isUserInRole("admin"));
        //System.out.println(request.isUserInRole("user"));

//        if (request.isUserInRole("ROLE_ADMIN"))
//           return "Hello?";
//        else
//            return "Goodbye!";
    }

    private List<Ticket> getUserTickets(User user) {
        return ticketRepository.findAllByOwner(user).stream().map(Ticket::flat).collect(Collectors.toList());
    }

    private List<Ticket> getActiveTickets() {
        return ticketRepository.findAllByStatusEquals(Ticket.Status.SENT).stream().map(Ticket::flat).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseBody
    public String createNewTicket(@RequestBody Ticket ticket, HttpServletRequest request) {
        request.getRemoteUser();
        return "";
    }


}
