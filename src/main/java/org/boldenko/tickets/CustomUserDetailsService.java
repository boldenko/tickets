package org.boldenko.tickets;

import lombok.RequiredArgsConstructor;
import org.boldenko.tickets.access.User;
import org.boldenko.tickets.access.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


@Service
@Transactional
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> found = userRepository.findByUsername(s);
        if (found.isPresent())
            return found.get();
        else
            throw new UsernameNotFoundException("User '" + s + "' not found");
    }
}
