package org.boldenko.tickets.data;

import org.boldenko.tickets.access.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {

    List<Ticket> findAllByOwner(User owner);

    List<Ticket> findAllByStatusEquals(Ticket.Status status);

}
