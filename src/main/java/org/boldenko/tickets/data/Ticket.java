package org.boldenko.tickets.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.boldenko.tickets.access.User;

import javax.persistence.*;

@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Ticket {
    public enum Status {
        DRAFT,
        SENT,
        ACCEPTED,
        DECLINED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private User owner;

    private String article = "";
    private Status status = Status.DRAFT;

    public Ticket flat() {
        Ticket flat = new Ticket();
        flat.setId(id);
        flat.setStatus(status);
        flat.setArticle(article);
        User flatUser = new User();
        flatUser.setId(owner.getId());
        flatUser.setUsername(owner.getUsername());
        flat.setOwner(flatUser);
        return flat;
    }

}
