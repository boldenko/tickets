package org.boldenko.tickets.access;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
public class Role implements GrantedAuthority {
    public final static String USER = "ROLE_USER";
    public final static String ADMIN = "ROLE_ADMIN";
    public final static String OPERATOR = "ROLE_OPERATOR";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Collection<User> users;

    @Override
    public String getAuthority() {
        return name;
    }
}
